<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<c:forEach items="${persons}" var="person">
		<c:out value="${person.firstName}"></c:out>
		<c:out value="${person.lastName}"></c:out>
		</br>
	</c:forEach>
</body>
</html>
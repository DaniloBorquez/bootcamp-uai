package com.example.services;

import java.util.List;

import org.springframework.stereotype.Service;

import com.example.models.Person;
import com.example.repositories.PersonRepository;

@Service
public class PersonService {
	private final PersonRepository personRepository;

	public PersonService(PersonRepository personRepository) {
		this.personRepository = personRepository;
	}
	
	public Person createPerson(Person p) {
		return personRepository.save(p);
	}
	public List<Person> listAll(){
		return personRepository.findAll();
	}
}

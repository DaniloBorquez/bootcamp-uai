package com.example.controllers;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.models.Person;
import com.example.services.PersonService;

@Controller
public class PersonController {
	private final PersonService personService;
	public PersonController(PersonService personService) {
		this.personService = personService;
	}
	
	@RequestMapping("/")
	public String index(Model model) {
		List<Person> p = this.personService.listAll();
		model.addAttribute("persons", p);
		return "asd.jsp";
	}
	
	@RequestMapping("/add/{name}/{ap}")
	public String insertar(Model model,@PathVariable("name")String name, @PathVariable("ap")String lastName) {
		Person p = new Person();
		p.setFirstName(name);
		p.setLastName(lastName);
		personService.createPerson(p);
		return "redirect:/";
	}
}

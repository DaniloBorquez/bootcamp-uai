import java.util.Scanner;

public class EjercicioTest {
    public static void main(String[] args) {
        Ejercicio e = new Ejercicio();
        Scanner sc = new Scanner(System.in);
        String input;
        System.out.print("Ingrese número de bits: ");
        input = sc.nextLine();
        System.out.println(
                "El máximo número representado por " + input + " es " + e.maximoNumero(Integer.valueOf(input)));
        sc.close();
    }
}
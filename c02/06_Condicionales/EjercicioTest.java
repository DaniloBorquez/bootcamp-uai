public class EjercicioTest {
    public static void main(String[] args) {
        Ejercicio e = new Ejercicio();

        System.out.println(e.divisible(2));
        System.out.println(e.divisible(3));
        System.out.println(e.divisible(4));
        System.out.println(e.divisible(5));
        System.out.println(e.divisible(6));
        System.out.println(e.divisible(30));
        System.out.println(e.divisible(11));

        System.out.println(e.existe("ho", "hola"));
        System.out.println(e.existe("ca", "saca"));
        System.out.println(e.existe("ta", "palta"));
        System.out.println(e.existe("ga", "gota"));
    }
}
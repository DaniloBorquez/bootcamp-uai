public class Ejercicio {
    /**
     * Divisible por 2, 3 o 5
     * 
     * @param valor Un valor para comparar
     * @return Un string mencionando si es divisible
     */
    public String divisible(int valor) {
        String mensaje = "El valor " + String.valueOf(valor) + " es divisible por: ";
        boolean esDivisible = false;
        if (valor % 2 == 0) {
            mensaje += 2 + " ";
            esDivisible = true;
        }
        if (valor % 3 == 0) {
            mensaje += 3 + " ";
            esDivisible = true;
        }
        if (valor % 5 == 0) {
            mensaje += 5 + " ";
            esDivisible = true;
        }
        if (!esDivisible) {
            mensaje = "El número no es divisible por 2, 3 o 5";
        }
        return mensaje;
    }

    public boolean existe(String aguja, String pajar) {
        // return pajar.contains(aguja);
        boolean a = pajar.indexOf(aguja) != -1;
        return a;
    }
}
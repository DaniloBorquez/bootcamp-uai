package com.example.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.example.models.Person;

@Controller
public class ControllerHome {
	@RequestMapping("/")
	public String abc(Model model, RedirectAttributes redirect) {
		Person p = new Person();
		p.setName("Daniela");
		model.addAttribute("estudiante", "Danilo");
		model.addAttribute("persona", p);
		System.out.println("Dentro de controller");
		redirect.addFlashAttribute("error", "NEW ATTRIBUTE");
		
		return "hola.jsp";
	}
}

package pojo;

public class Alumno implements java.io.Serializable {
	private String nombre;
	private String apellido;
	private float nota;
	java.util.ArrayList<Integer> h ;
	
	public Alumno() {
		
	}
	
	public double calculaNota() {
		return 3.4;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public float getNota() {
		return nota;
	}

	public void setNota(float nota) {
		this.nota = nota;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return super.toString();
	}
	
}

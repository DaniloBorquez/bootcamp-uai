public abstract class Fruta implements Comida {
    protected String color;
    protected String nombre;
    protected int tamanio;

    public Fruta(String color, String nombre, int tamanio) {
        this.color = color;
        this.nombre = nombre;
        this.tamanio = tamanio;
    }

    public Fruta() {
        this.color = "rojo";
        this.nombre = "sin nombre";
        this.tamanio = 0;
    }

    public String getColor() {
        return this.color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getTamanio() {
        return this.tamanio;
    }

    public void setTamanio(int tamanio) {
        this.tamanio = tamanio;
    }

}
import java.util.ArrayList;

/**
 * Libro
 */
public class Libro {

    private String titulo;
    ArrayList<Autor> autores;

    public Libro() {
        this.autores = new ArrayList<>();
    }

    public String getTitulo() {
        return this.titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public ArrayList<Autor> getAutores() {
        return this.autores;
    }

    public void setAutores(ArrayList<Autor> autores) {
        this.autores = autores;
    }

    public void addAutor(Autor autor) {
        this.autores.add(autor);
        autor.addLibro(this);
    }

    public String toString() {
        String retorno = "";
        retorno += "Título: " + this.titulo;
        retorno += "\nAutores: ";
        for (Autor autor : autores) {
            retorno += autor.toString() + ",";
        }
        return retorno;
    }
}
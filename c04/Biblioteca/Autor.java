import java.util.ArrayList;

public class Autor {
    private String nombre;
    private String apellido;
    private ArrayList<Libro> libros;

    public Autor() {
        this.libros = new ArrayList<>();
    }

    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return this.apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String toString() {
        String retorno = "";
        retorno += "nombre: " + this.nombre + " " + this.apellido;
        retorno += "\nLibros: ";
        for (Libro libro : libros) {
            retorno += libro.getTitulo() + ",";
        }
        return retorno;
    }

    public void addLibro(Libro libro) {
        this.libros.add(libro);
    }

    public ArrayList<Libro> getLibros() {
        return this.libros;
    }

    public void setLibros(ArrayList<Libro> libros) {
        this.libros = libros;
    }

}
public class Biblioteca {
    public static void main(String[] args) {
        Autor a1 = new Autor();
        Autor a2 = new Autor();
        Autor a3 = new Autor();
        Libro l1 = new Libro();
        Libro l2 = new Libro();
        Estante e1 = new Estante();
        Estante e2 = new Estante();

        a1.setNombre("Danilo");
        a1.setApellido("Borquez");
        a2.setNombre("Daniel");
        a2.setApellido("Borquez");
        a3.setNombre("Vale");
        a3.setApellido("Gonzalez");

        l1.setTitulo("Quijote");
        l1.addAutor(a1);
        l2.setTitulo("Papelucho");
        l2.addAutor(a2);

        e1.setId(1);
        e2.setId(2);
        e1.addLibro(l1);
        e2.addLibro(l2);

        System.out.println(a1);
        System.out.println(a2);
        System.out.println(a3);
        System.out.println(l1);
        System.out.println(e1);
        System.out.println(e2);
    }
}
import java.util.ArrayList;

/**
 * Estante
 */
public class Estante {

    private int id;
    ArrayList<Libro> libros;

    public Estante() {
        this.libros = new ArrayList<>();
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ArrayList<Libro> getLibros() {
        return this.libros;
    }

    public void setLibros(ArrayList<Libro> libros) {
        this.libros = libros;
    }

    public String toString() {
        return "ID: " + id;
    }

    public void addLibro(Libro libro) {
        this.libros.add(libro);
    }
}
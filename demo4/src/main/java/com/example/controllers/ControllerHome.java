package com.example.controllers;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class ControllerHome {

	@RequestMapping("/hola")
	public String index(HttpSession session) {
		session.setAttribute("nombre", "Danilo");
		return "index.jsp";
	}
	
	@RequestMapping("/chao")
	public String c(HttpSession session, Model model) {
		String name = (String)session.getAttribute("nombre");
		model.addAttribute("name", name);
		return "chao.jsp";
	}
	
	@RequestMapping(value="/login", method=RequestMethod.POST)
	public String login(@RequestParam("username")String user, @RequestParam("password")String pass) {
		System.out.println("Desde el post "+user + " "+pass);
		return "redirect:/hola";
	}
}

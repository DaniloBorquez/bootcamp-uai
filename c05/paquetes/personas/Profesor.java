package personas;

public class Profesor extends Persona {
    private String area;

    public Profesor() {
        super();
        this.area = "area no definida";
    }

    public Profesor(String nombre) {
        super(nombre);
        this.area = "area no definida";
    }

    public Profesor(String nombre, String apellido) {
        super(nombre, apellido);
        this.area = "area no definida";
    }

    public Profesor(String nombre, String apellido, String area) {
        super(nombre, apellido);
        this.area = area;
    }

    @Override
    public String toString() {
        return "Profesor: " + this.nombre + " " + this.apellido;
    }

}
package personas;

public class Persona {
    protected String nombre;
    protected String apellido;

    public Persona() {
        this.nombre = "No tengo nombre aún";
        this.apellido = "No tengo apellido aún";
    }

    public Persona(String nombre) {
        this.nombre = nombre;
        this.apellido = "No tengo apellido aún";
    }

    public Persona(String nombre, String apellido) {
        this.nombre = nombre;
        this.apellido = apellido;
    }

    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return this.apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String toString() {
        return this.nombre + " " + this.apellido;
    }

}
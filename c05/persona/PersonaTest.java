public class PersonaTest {

    public static void main(String[] args) {
        Persona p1 = new Persona();
        Persona p2 = new Persona("Danilo");
        Persona p3 = new Persona("Danilo", "Borquez");
        Profesor pp1 = new Profesor();

        System.out.println(p1);
        System.out.println(p2);
        System.out.println(p3);
        System.out.println(pp1);
    }
}
package com.example.controllers;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controlador {

	@RequestMapping("/hola/{nombre}/{apellido}")
	public String hola(@PathVariable("apellido") String a, @PathVariable("nombre") String n) {
		return "hola " + n + " " + a;
	}

	@RequestMapping("/chao")
	public String noNecesariamenteElMismoNombre() {
		return "chao ";
	}
}

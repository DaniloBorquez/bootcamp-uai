package com.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Bbdd1Application {

	public static void main(String[] args) {
		SpringApplication.run(Bbdd1Application.class, args);
	}

}

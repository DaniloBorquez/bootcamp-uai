package com.example.controllers;



import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.models.Person;
import com.example.services.PersonService;

@Controller
public class PersonController {

	private final PersonService personService;

	public PersonController(PersonService personService) {
		this.personService = personService;
	}

	@RequestMapping("/")
	public String index(Model model) {
		List<Person> persons = this.personService.allPersons();
		model.addAttribute("persons", persons);
		return "index.jsp";
	}
	
	@RequestMapping("/add/{name}/{lastName}")
	public String add(@PathVariable("name")String name,@PathVariable("lastName")String lastName) {
		Person p = new Person();
		p.setLastName(lastName);
		p.setName(name);
		this.personService.createPerson(p);
		return "redirect:/";
	}
}

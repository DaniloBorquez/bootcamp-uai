package com.example.services;

import java.util.List;

import org.springframework.stereotype.Service;

import com.example.models.Person;
import com.example.repositories.PersonRepository;

@Service
public class PersonService {
	private final PersonRepository personRepository;
	
	public PersonService(PersonRepository personRepository) {
		this.personRepository = personRepository;
	}
	
	public List<Person> allPersons(){
		return this.personRepository.findAll();
	}
	
	public Person createPerson(Person p) {
		return this.personRepository.save(p);
	}
}

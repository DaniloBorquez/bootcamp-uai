import java.util.ArrayList;
import java.util.Random;

public class ArregloTest {
    public static void main(String[] args) {
        ArrayList<Integer> arreglo = new ArrayList<>();
        EjercicioUno a = new EjercicioUno();
        Random r = new Random();
        int n = 20;
        for (int i = 0; i < n; i++) {
            arreglo.add(r.nextInt(20));
        }
        for (int i = 0; i < n; i++) {
            System.out.print(arreglo.get(i) + " ");
        }
        System.out.println();
        System.out.println(a.cuentaPares(arreglo));
    }
}
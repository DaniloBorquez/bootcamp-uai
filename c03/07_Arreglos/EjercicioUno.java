import java.util.ArrayList;

public class EjercicioUno {
    public int cuentaPares(ArrayList<Integer> lista) {
        int count = 0;
        for (int i = 0; i < lista.size(); i++) {
            if (lista.get(i) % 2 == 0) {
                count++;
            }
        }
        return count;
    }
}
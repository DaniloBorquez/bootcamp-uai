import java.util.HashMap;

public class EjercicioDos {
    // Key: capital
    // contenido : pais
    // public boolean agregarPais(String pais, String capital,
    // HashMap<String,String> paises)
    // verdaero si pais ya está en hashmap. Falso en caso contrario

    public boolean agregarPais(String pais, String capital, HashMap<String, String> paises) {
        if (paises.containsKey(capital)) {
            return true;
        } else {
            paises.put(capital, pais);
            return false;
        }
    }

    public String menu() {
        return "Elija la opción que desee:\n1.- Agregar pais\n2.- Mostrar datos ingresados\n3.- Salir";
    }
}
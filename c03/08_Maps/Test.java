import java.util.HashMap;
import java.util.Scanner;
import java.util.Set;

public class Test {
    public static void main(String[] args) {
        EjercicioDos e = new EjercicioDos();
        HashMap<String, String> paises = new HashMap<>();
        Scanner sc = new Scanner(System.in);
        String pais;
        String capital;
        int opcion = -1;
        while (opcion != 3) {
            System.out.println(e.menu());
            opcion = Integer.valueOf(sc.nextLine());
            // System.out.println("Elegiste la opción " + opcion);
            switch (opcion) {
            case 1:
                System.out.print("Ingresa pais: ");
                pais = sc.nextLine();
                System.out.print("Ingresa capital: ");
                capital = sc.nextLine();
                e.agregarPais(pais, capital, paises);
                break;
            case 2:
                Set<String> indices = paises.keySet();
                for (String c : indices) {
                    System.out.print(c + ": " + paises.get(c) + "\n");
                }
            default:
                break;
            }
        }
        sc.close();
    }
}
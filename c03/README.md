# Enunciados de ejercicios

1.  Hacer un método cuya firma sea la que se muestra a continuación, y que retorne la cantidad de enteros que son pares

``` Java
public int cuentaPares(ArrayList<Integer> lista)
```
2.  Hacer un método que permita agregar un país a un HashMap con su capital como Key. 
``` Java
public boolean agregarPais(String pais, String capital, HashMap<String,String> paises)
```
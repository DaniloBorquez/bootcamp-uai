package org.danilo.animales;

import org.danilo.interfaces.Suena;

abstract public class Animal implements Suena {
	protected int numeroDePatas;

	@Override
	abstract public void hacerSonido();

	public int getNumeroDePatas() {
		return numeroDePatas;
	}

	public void setNumeroDePatas(int numeroDePatas) {
		this.numeroDePatas = numeroDePatas;
	}

}

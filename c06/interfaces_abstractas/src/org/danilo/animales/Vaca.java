package org.danilo.animales;

import org.danilo.interfaces.Comer;

public class Vaca extends Animal implements Comer{

	@Override
	public void hacerSonido() {
		System.out.println("Soy vaca y hago Muuuuu");
	}

	@Override
	public void comer() {
		System.out.println("La vaca està comiendo!");
	}

}

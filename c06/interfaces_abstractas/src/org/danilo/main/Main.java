package org.danilo.main;

import java.util.ArrayList;

import org.danilo.animales.*;
import org.danilo.interfaces.Suena;

public class Main {

	public static void main(String[] args) {
		ArrayList<Suena> sonoros = new ArrayList<>();
		sonoros.add(new Perro());
		sonoros.add(new Vaca());
		for(Suena s: sonoros) {
			s.hacerSonido();
		}
	}

}
